FROM python:3.8.5-alpine
RUN apk --update add bash nano
COPY . .
RUN pip install -r requirements.txt

EXPOSE 5000

CMD [ "python", "main.py" ]