import requests
import json
from flask import Flask, jsonify, request


def request_headers():
    url = "https://auth.riotgames.com/api/v1/authorization"

    payload="{\r\n   \"client_id\":\"play-valorant-web-prod\",\r\n   \"nonce\":\"1\",\r\n   \"redirect_uri\":\"https://beta.playvalorant.com/opt_in\",\r\n   \"response_type\":\"token id_token\",\r\n   \"scope\":\"account openid\",\r\n   \"withCredentials\": true\r\n}"
    headers = {
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    #response.headers.add("Access-Control-Allow-Origin", "*")
    return str(response.headers)


def authenticate(aisd, username, password):
    aisd = aisd.replace("=", "%3D")
    url = "https://auth.riotgames.com/api/v1/authorization"
    payload="{\"type\":\"auth\",\"username\": \""+ username + "\", \"password\":\"" + password + "\"}"
    print(payload)
    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'asid=' + aisd + '; Path=/; Domain=auth.riotgames.com; Secure; HttpOnly;'
    }
    response = requests.request("PUT", url, headers=headers, data=payload)
    return str(response.text)

app = Flask(__name__)


@app.route('/')
def index():
    res = jsonify(request_headers())
    res.headers.add("Access-Control-Allow-Origin", "*")
    return res

@app.route('/authenticate')
def authenticate_page():
    res = jsonify(authenticate(aisd = request.args.get('aisd'), username = request.args.get('username'), password = request.args.get('password')))
    res.headers.add("Access-Control-Allow-Origin", "*")
    return res


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
